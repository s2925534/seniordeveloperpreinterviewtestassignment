<?php


class LoginTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testLogin()
    {
        $response = $this->call('POST', '/login', [
            'username' => 'demo',
            'password' => 'pwd1234',
        ]);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('login', $response->original->name());
    }

}
