<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PurchaseOrderService extends Controller
{

    public function isLogin(Request $request){
        $userController = new UserController();
        return $userController->login($request);
    }
    /**
     * @param $ids
     * @return mixed
     */
    public function calculateTotals($ids)
    {
        $ids = explode(',', $ids['purchase_order_ids']);

        $purchase_order_products = $this->setPurchaseOrderProducts($ids);
        $productTypes = $this->setProductTypes($purchase_order_products);
        $individualTotals = $this->setIndividualTotals($purchase_order_products);
        $groupTotals = $this->initializeGroups($productTypes);
        $groupTotals = $this->calculateTotalPerGroup($individualTotals, $groupTotals);
        $records = $this->setRecordsFormat($groupTotals);

        return $records;
    }


    /**
     * @param $criteria
     * @param null $url
     * @return mixed
     */
    public function getExternalAPI($criteria, $url = null)
    {
        $url = $url !== null && isset($criteria['id']) ? $url : 'https://api.cartoncloud.com.au/CartonCloud_Demo/PurchaseOrders/' . $criteria['id'] . '?version=5&associated=true';
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Authorization: Basic ' . base64_encode($criteria['username'] . ':' . $criteria['password'])]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $res = json_decode($result, true);

        return $res;
    }

    /**
     * @param $type
     * @param $unit_quantity_initial
     * @param $product
     * @return int
     */
    private function calculateTotalPerItem($type, $unit_quantity_initial, $product)
    {
        switch ($type) {
            // By Weight	sum(unit_quantity_initial x Product.weight)
            case 1:
            case 3:
                $total = $unit_quantity_initial * $product['weight'];
                break;

            // By Volume	sum(unit_quantity_initial x Product.volume)
            case 2:
                $total = $unit_quantity_initial * $product['volume'];
                break;

            default:
                $total = 0;
        }
        return $total;
    }

    /**
     * @param $purchase_order_products
     * @param array $productTypes
     * @return array
     */
    public function setProductTypes($purchase_order_products, $productTypes = [])
    {
        foreach ($purchase_order_products as $k => $item) {
            array_push($productTypes, $item['product_type_id']);
        }
        return array_unique($productTypes, SORT_REGULAR);
    }

    /**
     * @param $productTypes
     * @param array $groupTotals
     * @return array
     */
    public function initializeGroups($productTypes, $groupTotals = [])
    {
        foreach ($productTypes as $key => $item) {
            $groupTotals[$item] = 0.00;
        }
        return $groupTotals;
    }

    /**
     * @param $groupTotals
     * @param array $records
     * @return array
     */
    public function setRecordsFormat($groupTotals, $records = [])
    {
        foreach ($groupTotals as $key => $item) {
            array_push($records, ['product_type_id' => $key, 'total' => $item]);
        }
        return $records;
    }

    /**
     * @param $individualTotals
     * @param $groupTotals
     */
    protected function calculateTotalPerGroup($individualTotals, $groupTotals)
    {
        foreach ($individualTotals as $k => $item) {
            $groupTotals[$item['product_type_id']] += $item['total'];
        }
        return $groupTotals;
    }

    /**
     * @param $ids
     * @param array $purchase_order_products
     * @return array
     */
    protected function setPurchaseOrderProducts($ids, $purchase_order_products = [])
    {
        foreach ($ids as $key => $id) {
            $response = $this->getExternalAPI(
                [
                    'username' => 'interview-test@cartoncloud.com.au',
                    'password' => 'test123456',
                    'id' => $id
                ]
            );
            // set all products in a single array
            if (isset($response['data']['PurchaseOrderProduct'])) {
                foreach ($response['data']['PurchaseOrderProduct'] as $k => $item) {
                    array_push($purchase_order_products, $response['data']['PurchaseOrderProduct'][$k]);
                }
            }
        }
        return $purchase_order_products;
    }

    /**
     * @param $purchase_order_products
     * @param array $individualTotals
     * @return array
     */
    protected function setIndividualTotals($purchase_order_products, $individualTotals = [])
    {
        foreach ($purchase_order_products as $k => $item) {
            $individualTotals[$k] = ['product_type_id' => $item['product_type_id'], 'total' => $this->calculateTotalPerItem($item['product_type_id'], $item['unit_quantity_initial'], $item['Product'])];
        }
        return $individualTotals;
    }
}
