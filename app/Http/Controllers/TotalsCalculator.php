<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TotalsCalculator extends PurchaseOrderService
{

    /**
     * @param Request $request
     * @return string|void
     */
    public function test(Request $request) {
        if(!$this->isLogin($request)){
            return 'You are not logged in. Please provide username and password.';
        }
        try{
            return $this->generateReport($request->all());
        }catch (\Exception $exception){
            return $exception->getMessage();
        }
    }

    /**
     * @param array $ids
     */
    public function generateReport(array $ids) {
        $service = new PurchaseOrderService();
        $result = $service->calculateTotals($ids);
        foreach($result as $record) {
            echo "Product Type " . $record['product_type_id'] . " has total of " . $record['total']."\n";
        }
    }

}
