#      Senior Developer Pre-Interview Test Assignment Lumen based

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/lumen-framework/v/stable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://poser.pugx.org/laravel/lumen-framework/license.svg)](https://packagist.org/packages/laravel/lumen-framework)

This project was conceptualized with the Laravel Lumen which is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

## Purpose of this test

The idea of this test is for the developer to show some required skills prior to first interview of employment.

Unit Testing - file can be found under /tests folder of this project
Controllers with required developed code and required unchanged code can be found under the folder /app/Http/Controllers


## Clarification
 As pointed out by the provide documentation this could be developed within a framework and some adjustments were made as per Laravel Lumen framework standards such as the name of the class (controller) TotalsCalculator which changed to TotalsCalculatorController and its under the namespace App\Http\Controllers.
 The actual code provided remained unchanged as required and show below:
 

    /**
     * @param array $ids
     */
    public function generateReport(array $ids) {
        $service = new PurchaseOrderService();
        $result = $service->calculateTotals($ids);
        foreach($result as $record) {
            echo "Product Type " . $record['product_type_id'] . " has total of " . $record['total']."\n";
        }
    }

Migration file can be found under /database/migrations
user model class can be found under /app/User.php

## Installation
Due to the use of the framework the app must be installed.
1. Pull the repo
2. point your wen server http requests to the folder /public of this repo
3. Run composer install while under the repo main root folder where the composer.json can be found
4. Run php artisan migrate (this step is just for framework proper setup and no real user information will need to be populated)
5. You are all set

## Usage
For tests case of login please refer to files under /tests folder.
For test of the required function "test" please use Google Postman as show in pics below:

Without credentials

![Scheme](public/images/Postman-missing-credentials.png)

With credentials

![Scheme](public/images/Postman-with-credentials.png)

## License

The sample work is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
